You can include stylesheets with the "<link>" tag
You can include javascript with the "<script>" tag
You can add an icon for the page using:
	<link rel="icon" type="image/png" href="./images/favico.png" />

To check that jQuery is loaded, check the following value in the browser's javascript console
	(e.g. the variable could return 2.1.4)
	$.fn.jquery

CTRL+SHIFT+R Refreshes the page on Windows
Option+Shift+R Refreshes the page on MacOS

Explore other HTML tags
http://www.w3schools.com/html/default.asp